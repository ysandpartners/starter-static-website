import Glide from '../lib/glide.js';

// Hamburger menu
{
  const hbMenu = {
    el: {
      headerLogoEl: document.querySelector('.nav-sp__logo'),
      menuEl: document.querySelector('.js_nav-sp'),
      openBtnEl: document.querySelector('.js_nav-sp-open'),
      closeBtnEl: document.querySelector('.js_nav-sp-close'),
      navLinkEls: document.querySelectorAll('.js_nav-sp .js_link')
    },

    init() {
      this.openHandler();
      this.closeHandler();
      this.linkHandler();
    },

    openHandler() {
      this.el.openBtnEl.addEventListener('click', (event) => {
        event.preventDefault();
        this.openMenu();
        return 0;
      });
    },

    closeHandler() {
      this.el.closeBtnEl.addEventListener('click', (event) => {
        event.preventDefault();
        this.closeMenu();
        return 0;
      });
    },

    openMenu() {
      this.el.menuEl.classList.add('active');
      this.el.headerLogoEl.classList.add('inactive');
    },

    closeMenu() {
      this.el.menuEl.classList.remove('active');
      this.el.headerLogoEl.classList.remove('inactive');
    },

    linkHandler() {
      this.el.navLinkEls.forEach((navLink) => {
        navLink.addEventListener('click', () => {
          this.closeMenu();
        });
      });
    }
  };

  if (document.querySelector('.js_nav-sp')) {
    hbMenu.init();
  }
}

// Success stories slider
{
  const storiesSlider = {
    el: {
      sliderEl: document.querySelector('.js_storiesSlider')
    },

    init() {
      this.runSlider();
    },

    runSlider() {
      const glide = new Glide(this.el.sliderEl, {
        type: 'carousel',
        perView: 1.8,
        focusAt: 'center',
        gap: 5,
        animationDuration: 400,
        autoplay: false,
        keyboard: true,
        breakpoints: {
          767: {
            perView: 1
          }
        }
      });

      glide.mount();
    }
  };

  if (document.querySelector('.js_storiesSlider')) {
    storiesSlider.init();
  }
}