import Jump from '../lib/jump.js';

// Smooth scrolling links
{
  const smoothScrolllingLinks = {
    el: {
      links: document.querySelectorAll('.js_link')
    },

    init() {
      this.clickHandler();
    },

    clickHandler() {
      this.el.links.forEach((link) => {
        link.addEventListener('click', (event) => {
          event.preventDefault();
          this.goTo(link);
          return 0;
        });
      });
    },

    goTo(link) {
      // Target ID
      const linkID = link.getAttribute('href');

      // Offset
      let headerHeight = 0;

      if (window.innerWidth < 768) {
        headerHeight = document.querySelector('.header-sp').offsetHeight;
        headerHeight += 5;
      } else {
        headerHeight = document.querySelector('.header-pc').offsetHeight;
        headerHeight += 10;
      }

      // Use Jump.js
      Jump(linkID, {
        duration: 700,
        offset: -headerHeight,
        a11y: false,
        //easing: easeInOutQuad,
        //callback: () => console.log('jump completed'),
      });
    }
  }

  if (document.querySelector('.js_link')) {
    smoothScrolllingLinks.init();
  }
}