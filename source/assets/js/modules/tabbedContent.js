// Tabbed/accordion content section
{
  const tabbedContent = {
    el: {
      accordionBtnEls: document.querySelectorAll('.js_acr-tab'),
      accordionBtnClass: '.js_acr-tab',
      accordionSectionClass: '.js_acr-content'
    },

    init() {
      this.clickHandler();
    },

    clickHandler() {
      const _this = this;

      this.el.accordionBtnEls.forEach((btnEl) => {
        btnEl.addEventListener('click', (event) => {
          event.preventDefault();

          _this.openContent(btnEl);

          return 0;
        });
      });
    },

    openContent(btnEl) {
      const currentAcr = btnEl.closest('.js_acr');
      const contentID = btnEl.getAttribute('href');
      const content = currentAcr.querySelector(contentID);

      this.closeContent(currentAcr);

      btnEl.classList.add('active');
      content.classList.add('active');
    },

    closeContent(currentAcr) {
      const accordionBtnEls = currentAcr.querySelectorAll(this.el.accordionBtnClass);
      const accordionSectionEls = currentAcr.querySelectorAll(this.el.accordionSectionClass);

      accordionBtnEls.forEach((btnEl) => {
        btnEl.classList.remove('active');
      });
      accordionSectionEls.forEach((sectionEl) => {
        sectionEl.classList.remove('active');
      });
    }
  };

  if (document.querySelector('.js_acr')) {
    tabbedContent.init();
  }
}