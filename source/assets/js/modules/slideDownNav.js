// Show and hide global navigation on scroll
{
  const slideDownNav = {
    el: {
      headerEls: document.querySelectorAll('.js_header'),
      headerClass: '.js_header',
      activateSectionClass: '.js_header-activate'
    },

    config: {
      active: false
    },

    init() {
      this.toggleNav(window.scrollY);
      this.scrollHandler();
    },

    scrollHandler() {
      const _this = this;

      let lastKnownScrollPosition = 0;
      let ticking = false;

      function doWhileScrolling(scrollPos) {
        _this.toggleNav(scrollPos);
      }

      document.addEventListener('scroll', function () {
        lastKnownScrollPosition = window.scrollY;

        if (!ticking) {
          window.requestAnimationFrame(function () {
            doWhileScrolling(lastKnownScrollPosition);
            ticking = false;
          });

          ticking = true;
        }
      });
    },

    toggleNav(scrollPos) {
      // Get activate position
      const activateElementPos = document.querySelector(
        this.el.activateSectionClass
      ).offsetTop;
      const headerHeight = document.querySelector(
        this.el.headerClass
      ).offsetHeight;
      const activatePos = activateElementPos - headerHeight;

      // Activate header
      if (scrollPos >= activatePos && this.config.active !== true) {
        if (this.config.active !== true) {
          this.slideDown();

          this.config.active = true;
        } else {
          this.slideDown();
        }

        this.config.active = true;
      }

      // Deactivate header
      if (scrollPos < activatePos && this.config.active !== false) {
        this.slideUp();

        this.config.active = false;
      }
    },

    slideDown() {
      this.el.headerEls.forEach(function (headerEl) {
        headerEl.classList.add('active');
      });
    },

    slideUp() {
      this.el.headerEls.forEach(function (headerEl) {
        headerEl.classList.remove('active');
      });
    }
  };

  // Only enable the sticky dropdown navigation on the top page
  if (document.querySelector('.js_header')) {
    slideDownNav.init();
  }
}