import Glide from '../lib/glide.js';

// Success stories slider
{
  const storiesSlider = {
    el: {
      sliderEl: document.querySelector('.js_storiesSlider')
    },

    init() {
      this.runSlider();
    },

    runSlider() {
      const glide = new Glide(this.el.sliderEl, {
        type: 'carousel',
        perView: 1.8,
        focusAt: 'center',
        gap: 5,
        animationDuration: 400,
        autoplay: false,
        keyboard: true,
        breakpoints: {
          767: {
            perView: 1
          }
        }
      });

      glide.mount();
    }
  };

  if (document.querySelector('.js_storiesSlider')) {
    storiesSlider.init();
  }
}