// --------------------------------------------
// Dependencies
// --------------------------------------------

import gulp from 'gulp';

import stylus from 'gulp-stylus';
import postcss from 'gulp-postcss';
import autoprefixer from 'autoprefixer';
import cssnano from 'cssnano';
import pxtorem from 'postcss-pxtorem';
import sourcemaps from 'gulp-sourcemaps';

import babel from 'gulp-babel';
import uglify from 'gulp-uglify';
import concat from 'gulp-concat';
import rename from 'gulp-rename';
import webpack from 'webpack-stream';

import imagemin from 'gulp-imagemin';

import nunjucksRender from 'gulp-nunjucks-render';
import browserSync from 'browser-sync';
import plumber from 'gulp-plumber';

// Paths
const src = 'source/',
  dest = 'build/';

const styleSrcMain = `${src}assets/styles/main.styl`,
  styleSrc = `${src}assets/styles/**/*.styl`,
  styleDest = `${dest}assets/css/`,
  scriptSrc = `${src}assets/js/*.js`,
  libSrc = `${src}assets/js/lib/*.js`,
  scriptDest = `${dest}assets/js/`,
  libDest = `${dest}assets/js/lib/`,
  htmlSrc = `${src}**/!(_)*.{html,php}`,
  htmlSrcPartials = `${src}**/*.{html,php}`,
  imgSrc = `${src}assets/img/**/*`,
  imgDest = `${dest}assets/img/`,
  imgFavicon = `${src}favicon.ico`,
  htmlWatch = `${dest}**/*.html`,
  styleWatch = `${dest}assets/css/*.css`,
  scriptWatch = `${dest}assets/js/*.js`;

// Error handler
const onError = function(err) {
  //play.sound('/Users/ysap_pm18/Code/audio/theoffice/problem.wav');
  console.log(err.toString());
  this.emit('end');
};

// --------------------------------------------
// Stand Alone Tasks
// --------------------------------------------

// Process styles
gulp.task('styles', () => {
  const plugins = [
    autoprefixer(),
    pxtorem(),
    cssnano()
  ];
  return gulp
    .src(styleSrcMain)
    .pipe(
      plumber({
        errorHandler: onError
      })
    )
    .pipe(sourcemaps.init())
    .pipe(stylus())
    .pipe(postcss(plugins))
    .pipe(
      rename({
        basename: 'styles',
        suffix: '.min'
      })
    )
    .pipe(sourcemaps.write('.'))

    .pipe(gulp.dest(styleDest));
});

// Process JS
gulp.task('scripts', () => {
  return gulp
    .src(scriptSrc)
    .pipe(
      webpack({
        config : require('./webpack.config.js')
      })
    )
    .pipe(gulp.dest(scriptDest));
});

// Process JS libraries
gulp.task('libraries', () => {
  return gulp
    .src(libSrc)
    .pipe(plumber())
    .pipe(uglify())
    .pipe(gulp.dest(libDest));
});

// Process HTML
gulp.task('nunjucks', () => {
  nunjucksRender.nunjucks.configure(src, {watch: false});
  return gulp
    .src(htmlSrc)
    .pipe(
      nunjucksRender({
        inheritExtension: true
      })
    )
    .pipe(gulp.dest(dest));
});

// Process images
gulp.task('images', () => {
  return gulp
    .src(imgSrc)
    .pipe(imagemin())
    .pipe(gulp.dest(imgDest));
});
gulp.task('favicon', () => {
  return gulp.src(imgFavicon).pipe(gulp.dest(dest));
});

// Watch for changes
gulp.task('watch', function() {
  // Serve files from the root of this project
  browserSync.init({
    server: {
      baseDir: dest
    },
    notify: false
  });

  gulp.watch(styleSrc, gulp.series('styles'));
  gulp.watch(scriptSrc, gulp.series('scripts'));
  //gulp.watch(libSrc, gulp.series('libraries'));
  gulp.watch(htmlSrcPartials, gulp.series('nunjucks'));
  gulp.watch(imgSrc, gulp.series('images'));
  gulp.watch(imgFavicon, gulp.series('favicon'));
  gulp
    .watch([htmlWatch, styleWatch, scriptWatch])
    .on('change', browserSync.reload);
});

// use default task to launch Browsersync and watch JS files
gulp.task(
  'default',
  gulp.series(
    'styles',
    'scripts',
    'nunjucks',
    'images',
    'favicon',
    'watch'
  )
);
