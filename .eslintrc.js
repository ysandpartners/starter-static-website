module.exports = {
  parser: 'babel-eslint',
  extends: ['airbnb', 'prettier'],
  plugins: ['prettier'],
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'script'
  },
  env: {
    browser: true,
    es6: true,
    jquery: true
  },
  rules: {
    'prettier/prettier': ['error'],
    'padded-blocks': 'off',
    'wrap-iife': ['error', 'inside'],
    'comma-dangle': ['error', 'never'],
    'func-names': 'off',
    strict: ['error', 'global'],
    'prefer-arrow-callback': 'off',
    'no-underscore-dangle': 'off'
  }
};
