// webpack.config.js

const path = require("path")

module.exports = {
  entry: {
    main: "./source/assets/js/main.js",
    minimal: "./source/assets/js/minimal.js",
  },
  output: {
    filename: "[name].min.js",
    path: path.resolve(__dirname, "./build/assets/js/"),
  },
  mode: 'production',
  // mode: 'development',
  // devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /node_modules/
      }
    ]
  },
}